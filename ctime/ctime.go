package ctime

import (
	"fmt"
	"os"
	"reflect"
	"strconv"
	"time"

	"github.com/araddon/dateparse"
	fs "gitlab.com/egi-pub/go-lib/filesystem"
)

var (
	timezone = ""
)

func Format(t interface{}) string {
	if timezone != "" {
		// NOTE:  This is very, very important to understand
		// time-parsing in go
		loc, err := time.LoadLocation(timezone)
		if err != nil {
			panic(err.Error())
		}
		time.Local = loc
	}
	var timedata string
	ty := reflect.TypeOf(t).String()
	switch ty {
	case "time.Time":
		ts := t.(time.Time)
		timedata = ts.String()
	default:
		timedata = t.(string)
	}
	if timedata == "" {
		return ""
	}
	pt, err := dateparse.ParseLocal(timedata)
	if err != nil {
		fmt.Println("Failed to parse time: ", err.Error())
	}

	return pt.Format(time.RFC850)
	// return fmt.Sprintf("%v", pt)

}

func TimeSinceSeconds(t time.Time) float64 {
	return time.Since(t).Seconds()
}

func TimeSinceMinutes(t time.Time) float64 {
	return time.Since(t).Minutes()
}

func TimeSinceHours(t time.Time) float64 {
	return time.Since(t).Hours()
}

func dbug(m string) {
	if os.Getenv("DEBUG") != "" {
		fmt.Println(m)
	}
	if os.Getenv("ACTIVE_JOB") != "" {
		name := os.Getenv("ACTIVE_JOB")
		path := "/var/log/c3r/jobs/" + name + ".log"
		fs.Append(path, Format(time.Now())+" :: "+m+"\n")
	}
}

func Uptime(t interface{}) string {
	if timezone != "" {
		// NOTE:  This is very, very important to understand
		// time-parsing in go
		loc, err := time.LoadLocation(timezone)
		if err != nil {
			panic(err.Error())
		}
		time.Local = loc
	}
	var timedata string
	ty := reflect.TypeOf(t).String()
	switch ty {
	case "time.Time":
		ts := t.(time.Time)
		timedata = ts.String()
	default:
		timedata = t.(string)
	}
	if timedata == "" {
		return ""
	}
	dbug("timedata: " + timedata)
	pt, err := dateparse.ParseLocal(timedata)
	if err != nil {
		fmt.Println("Failed to parse time: ", err.Error())
	}

	n := time.Now().Sub(pt)

	if ok := n.Hours(); ok > 24 {
		return strconv.FormatFloat(n.Hours(), 'f', 2, 64) + " day(s)"
	}

	if ok := n.Hours(); ok > 1 {
		return strconv.FormatFloat(n.Hours(), 'f', 2, 64) + " hour(s)"
	}

	if ok := n.Minutes(); ok > 1 {
		return strconv.FormatFloat(n.Minutes(), 'f', 2, 64) + " minute(s)"
	}

	if ok := n.Seconds(); ok > 1 {
		return strconv.FormatFloat(n.Seconds(), 'f', 2, 64) + " second(s)"
	}
	return n.String()
}
