package filesystem

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"
)

func FileExist(path string) bool {
	// // log := clog.ZapLogger()
	// if _, err := os.Stat(path); os.IsNotExist(err) {
	// 	// log.Error(err.Error())
	// 	return false
	// }
	// return true
	if _, err := os.Stat(path); err == nil {
		// exist
		return true
	}
	// not exist
	return false
}

// Return true if the given file exists
func FileExists(path string) bool {
	if _, err := os.Stat(path); err == nil {
		// exist
		return true
	}
	// not exist
	return false
}

// Return true if the given file does not exist
func FileNotExists(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		// not exist
		return true
	}
	// exist
	return false
}

// Return the canonical version of the given path, relative to the given base path. That is, if the given path is a
// relative path, assume it is relative to the given base path. A canonical path is an absolute path with all relative
// components (e.g. "../") fully resolved, which makes it safe to compare paths as strings.
func CanonicalPath(path string, basePath string) (string, error) {
	if !filepath.IsAbs(path) {
		path = JoinPath(basePath, path)
	}
	absPath, err := filepath.Abs(path)
	if err != nil {
		return "", err
	}

	return CleanPath(absPath), nil
}

func ListFiles(p string) ([]string, error) {
	var files []string

	root := p
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		files = append(files, path)
		return nil
	})
	if err != nil {
		return files, err
	}
	// for _, file := range files {
	//     fmt.Println(file)
	// }
	return files, nil
}

func ListFilesByType(p, t string) ([]string, error) {
	var files []string

	root := p
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		// filter by type
		_, file := filepath.Split(path)
		if strings.Contains(file, ".") && strings.Contains(file, t) {
			files = append(files, path)
		}
		return nil
	})
	if err != nil {
		return files, err
	}
	// for _, file := range files {
	//     fmt.Println(file)
	// }
	return files, nil
}

// Return the canonical version of the given paths, relative to the given base path. That is, if a given path is a
// relative path, assume it is relative to the given base path. A canonical path is an absolute path with all relative
// components (e.g. "../") fully resolved, which makes it safe to compare paths as strings.
func CanonicalPaths(paths []string, basePath string) ([]string, error) {
	canonicalPaths := []string{}

	for _, path := range paths {
		canonicalPath, err := CanonicalPath(path, basePath)
		if err != nil {
			return canonicalPaths, err
		}
		canonicalPaths = append(canonicalPaths, canonicalPath)
	}

	return canonicalPaths, nil
}

// Return true if the path points to a directory
func IsDir(path string) bool {
	fileInfo, err := os.Stat(path)
	return err == nil && fileInfo.IsDir()
}

// Return true if the path points to a file
func IsFile(path string) bool {
	fileInfo, err := os.Stat(path)
	return err == nil && !fileInfo.IsDir()
}

// Return the relative path you would have to take to get from basePath to path
func GetPathRelativeTo(path string, basePath string) (string, error) {
	if path == "" {
		path = "."
	}
	if basePath == "" {
		basePath = "."
	}

	inputFolderAbs, err := filepath.Abs(basePath)
	if err != nil {
		return "", err
	}

	fileAbs, err := filepath.Abs(path)
	if err != nil {
		return "", err
	}

	relPath, err := filepath.Rel(inputFolderAbs, fileAbs)
	if err != nil {
		return "", err
	}

	return filepath.ToSlash(relPath), nil
}

// Return the contents of the file at the given path as a string
func ReadFileAsString(path string) (string, error) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	return string(bytes), nil
}

// IsSymLink returns true if the given file is a symbolic link
// Per https://stackoverflow.com/a/18062079/2308858
func IsSymLink(path string) bool {
	fileInfo, err := os.Lstat(path)
	return err == nil && fileInfo.Mode()&os.ModeSymlink != 0
}

func PathContainsHiddenFileOrFolder(path string) bool {
	pathParts := strings.Split(path, string(filepath.Separator))
	for _, pathPart := range pathParts {
		if strings.HasPrefix(pathPart, ".") && pathPart != "." && pathPart != ".." {
			return true
		}
	}
	return false
}

// Copy a file from source to destination
func CopyFile(source string, destination string) error {
	contents, err := ioutil.ReadFile(source)
	if err != nil {
		return err
	}

	return WriteFileWithSamePermissions(source, destination, contents)
}

// Write a file to the given destination with the given contents using the same permissions as the file at source
func WriteFileWithSamePermissions(source string, destination string, contents []byte) error {
	fileInfo, err := os.Stat(source)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(destination, contents, fileInfo.Mode())
}

// Windows systems use \ as the path separator *nix uses /
// Use this function when joining paths to force the returned path to use / as the path separator
// This will improve cross-platform compatibility
func JoinPath(elem ...string) string {
	return filepath.ToSlash(filepath.Join(elem...))
}

// Use this function when cleaning paths to ensure the returned path uses / as the path separator to improve cross-platform compatibility
func CleanPath(path string) string {
	return filepath.ToSlash(filepath.Clean(path))
}

func Append(p string, d string) error {
	f, err := os.OpenFile(p,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()
	if _, err := f.WriteString(d + "\n"); err != nil {
		return err
	}
	return nil
}

func SetExecutePermission(p string) error {
	// log := clog.ZapLogger()
	if ok := FileExist(p); ok {

		// Ok("Running shell in host mode: " + command)
		cmd := exec.Command("chmod", "+x", p)
		cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
		// start := time.Now()
		time.AfterFunc(5*time.Second, func() {
			syscall.Kill(-cmd.Process.Pid, syscall.SIGKILL)
		})

		_, err := cmd.CombinedOutput()

		if err != nil {

			// Er("Error running command", errors.New(command))
			// log.Error(err.Error())
			return err
		}

	} else {
		return errors.New("File " + p + " does not exist")
	}
	return nil
}

func CreateSoftLink(target string, symlink string) {

	os.Symlink(target, symlink)
}

// CreateFile : Create file on local file system
func CreateFile(path string) error {
	// log := clog.ZapLogger()
	// Create folder structure for file if not already exist
	if _, err := os.Stat(path); os.IsNotExist(err) {
		dir, _ := filepath.Split(path)
		MkDir(dir)
	}
	// create file if not exists
	if _, err := os.Stat(path); os.IsNotExist(err) {
		var file, err = os.Create(path)
		// // log.WithFields(log.Fields{"Router": "CreateFile"}).Info("Creating File: "+ path)
		if err != nil {
			// log.Error("Error Creating File: " + err.Error())
			// log.Error(err.Error())
			return err
		}
		defer file.Close()
	}
	return nil
}

func CreateFileCtx(ctx context.Context, ch chan bool, path string) error {
	// log := clog.ZapLogger()
	defer func() {
		// // fmt.Printf("%s", "subroutine complete")
		ch <- true
	}()
	// Create folder structure for file if not already exist
	if _, err := os.Stat(path); os.IsNotExist(err) {
		dir, _ := filepath.Split(path)
		MkDir(dir)
	}
	// create file if not exists
	if _, err := os.Stat(path); os.IsNotExist(err) {
		var file, err = os.Create(path)
		// // log.WithFields(log.Fields{"Router": "CreateFile"}).Info("Creating File: "+ path)
		if err != nil {
			// log.Error("Error Creating File: " + err.Error())
			// log.Error(err.Error())
			return err
		}
		defer file.Close()
	}
	//Use a select statement to exit out if context expires
	select {
	case <-ctx.Done():
		//If context is cancelled, this case is selected
		//This can happen if the timeout doWorkContext expires or
		//doWorkContext calls cancelFunction or main calls cancelFunction
		//Free up resources that may no longer be needed because of aborting the work
		//Signal all the goroutines that should stop work (use channels)
		//Usually, you would send something on channel,
		//wait for goroutines to exit and then return
		//Or, use wait groups instead of channels for synchronization

		// fmt.Printf("%s", "subroutine canceled: Context expired")

	default:
		// fmt.Print("%s", "Job complete")
		func() {
			// // fmt.Printf("%s", "subroutine complete")
			ch <- true
		}()
	}
	return nil
}

// WriteFile : Write string to a file on local file system
func WriteFile(path string, contents string, permission os.FileMode) error {
	// log := clog.ZapLogger()
	// if _, err := os.Stat(path); os.IsNotExist(err) {
	// 	CreateFile(path)
	// 	err := ioutil.WriteFile(path, []byte(contents), permission)
	// 	if err != nil {
	// // 		log.Error("Error writing file: "+ err.Error())
	// 		return err
	// 	}
	// } else {
	// 	err := ioutil.WriteFile(path, []byte(contents), permission)
	// 	if err != nil {
	// // 		log.Error("Error writing file: "+ err.Error())
	// 		return err
	// 	}
	// }
	// return nil
	// Open a new file for writing only
	// fmt.Println("Ask to write a file: "+ path)
	file, err := os.OpenFile(
		path,
		os.O_WRONLY|os.O_TRUNC|os.O_CREATE,
		permission,
	)
	if err != nil {
		// log.Error(err.Error())
		return err
	}
	defer file.Close()

	// Write bytes to file
	byteSlice := []byte(contents)
	_, err = file.Write(byteSlice)
	if err != nil {
		// log.Error(err.Error())
		return err
	}
	return nil
}

func WriteFileCtx(ctx context.Context, ch chan bool, path string, contents string, permission os.FileMode) error {
	// log := clog.ZapLogger()
	defer func() {
		// // fmt.Printf("%s", "subroutine complete")
		ch <- true
	}()
	if _, err := os.Stat(path); os.IsNotExist(err) {
		CreateFile(path)
		err := ioutil.WriteFile(path, []byte(contents), permission)
		if err != nil {
			// log.Error("Error writing file: " + err.Error())
			return err
		}
	} else {
		err := ioutil.WriteFile(path, []byte(contents), permission)
		if err != nil {
			// log.Error("Error writing file: " + err.Error())
			return err
		}
	}
	//Use a select statement to exit out if context expires
	select {
	case <-ctx.Done():
		//If context is cancelled, this case is selected
		//This can happen if the timeout doWorkContext expires or
		//doWorkContext calls cancelFunction or main calls cancelFunction
		//Free up resources that may no longer be needed because of aborting the work
		//Signal all the goroutines that should stop work (use channels)
		//Usually, you would send something on channel,
		//wait for goroutines to exit and then return
		//Or, use wait groups instead of channels for synchronization

		// fmt.Printf("%s", "subroutine canceled: Context expired")

	default:
		// fmt.Print("%s", "Job complete")
		func() {
			// // fmt.Printf("%s", "subroutine complete")
			ch <- true
		}()
	}
	return nil
}

// ReadFile : Read file from local file system
func ReadFile(path string) []byte {
	// log := clog.ZapLogger()
	var content []byte
	if path == "" {
		// log.Error("Requested reading a file that has a blank path")
	} else {

		// detect if file exists
		var _, err = os.Stat(path)

		if os.IsNotExist(err) {
			// log.Error("Trying to read a file that does not exist: " + path)

			// No file exist at path to read
		} else {
			// File exist, read and return contents
			content, err = ioutil.ReadFile(path)
			if err != nil {
				// WriteFile(path, " ", 0666)
				// log.Error(err.Error())
			}
		}
	}
	// // log.WithFields(log.Fields{"router": "Filesystem"}).Info("read file")
	return content
}

func ReadFileCtx(ctx context.Context, ch chan bool, path string) []byte {
	// log := clog.ZapLogger()
	defer func() {
		// // fmt.Printf("%s", "subroutine complete")
		ch <- true
	}()
	var content []byte
	if path == "" {
		// log.Error("Requested reading a file that has a blank path")
	} else {
		// detect if file exists
		var _, err = os.Stat(path)

		if os.IsNotExist(err) {
			// log.Error("Trying to read a file that does not exist: " + path)

			// No file exist at path to read
		} else {
			// File exist, read and return contents
			content, err = ioutil.ReadFile(path)
			if err != nil {
				// WriteFile(path, " ", 0666)
				// log.Error(err.Error())
			}
		}
	}
	//Use a select statement to exit out if context expires
	select {
	case <-ctx.Done():
		//If context is cancelled, this case is selected
		//This can happen if the timeout doWorkContext expires or
		//doWorkContext calls cancelFunction or main calls cancelFunction
		//Free up resources that may no longer be needed because of aborting the work
		//Signal all the goroutines that should stop work (use channels)
		//Usually, you would send something on channel,
		//wait for goroutines to exit and then return
		//Or, use wait groups instead of channels for synchronization

		// fmt.Printf("%s", "subroutine canceled: Context expired")

	default:
		// fmt.Print("%s", "Job complete")
		func() {
			// // fmt.Printf("%s", "subroutine complete")
			ch <- true
		}()
	}
	// // log.WithFields(log.Fields{"router": "Filesystem"}).Info("read file")
	return content
}

// MkDir : Create directory on local file system
func MkDir(dir string) error {
	// log := clog.ZapLogger()
	// Make sure directory does not already exist before creating
	if _, MkDirErr := os.Stat(dir); os.IsNotExist(MkDirErr) {
		MkDirErr = os.MkdirAll(dir, 0755)
		if os.Getenv("DEBUG") != "" {
			// log.Info("Creating Directory: " + dir)
		}
	}
	return nil
}

// RmDir : Remove directory and all contents from local file system
func RmDir(dir string) error {
	// log := clog.ZapLogger()
	var _, err = os.Stat(dir)

	if os.IsNotExist(err) {
		// No folder exist to delete
	} else {
		err := os.RemoveAll(dir)
		if err != nil {
			if os.Getenv("DEBUG") != "" {

				// log.Error(err.Error())
			}
			return err
		}
		if os.Getenv("DEBUG") != "" {
			// log.Info("Deleting Director: " + dir)
		}
	}
	return nil
}

// DeleteFile : Delete file from local file system
func DeleteFile(path string) error {
	// log := clog.ZapLogger()

	// detect if file exists
	var _, err = os.Stat(path)

	// create file if not exists
	if os.IsNotExist(err) {
		var file, err = os.Create(path)
		if err != nil {
			if os.Getenv("DEBUG") != "" {
				// log.Error(err.Error())
			}
		}
		// fmt.Println(err, "Delete file: "+path) //okay to call os.exit()
		defer file.Close()
	}
	// delete file
	var err1 = os.Remove(path)
	// fmt.Println(err1, "Delete file")
	return err1
}

// ChangeFilePermissions : Set permission on a file
func ChangeFilePermissions(path string, permission os.FileMode) error {
	// log := clog.ZapLogger()
	if err := os.Chmod(path, permission); err != nil {
		// log.Error(err.Error())
		return err
	}
	return nil
}

// func StatTimes(name string) (atime, mtime, ctime time.Time, err error) {

// 	fi, err := os.Stat(name)
// 	if err != nil {
// 		return
// 	}
// 	mtime = fi.ModTime()
// 	stat := fi.Sys().(*syscall.Stat_t)
// 	atime = time.Unix(int64(stat.Atim.Sec), int64(stat.Atim.Nsec))
// 	ctime = time.Unix(int64(stat.Ctim.Sec), int64(stat.Ctim.Nsec))
// 	return
// }

// TempDir is like ioutil.TempDir, but more docker friendly
func TempDir(dir, prefix string) (name string, err error) {
	// log := clog.ZapLogger()
	// create a tempdir as normal
	name, err = ioutil.TempDir(dir, prefix)
	if err != nil {
		// log.Error(err.Error())
		return "", err
	}
	// on macOS $TMPDIR is typically /var/..., which is not mountable
	// /private/var/... is the mountable equivalent
	if runtime.GOOS == "darwin" && strings.HasPrefix(name, "/var/") {
		name = filepath.Join("/private", name)
	}
	return name, nil
}

var BUFFERSIZE int64

func copy(src, dst string, BUFFERSIZE int64) error {
	// log := clog.ZapLogger()
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return err
	}

	if !sourceFileStat.Mode().IsRegular() {
		// log.Error(err.Error())
		return fmt.Errorf("%s is not a regular file." + src)
	}

	source, err := os.Open(src)
	if err != nil {
		return err
	}
	defer source.Close()

	_, err = os.Stat(dst)
	if err == nil {
		return fmt.Errorf("File %s already exists.", dst)
	}

	destination, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer destination.Close()

	if err != nil {
		panic(err)
	}

	buf := make([]byte, BUFFERSIZE)
	for {
		n, err := source.Read(buf)
		if err != nil && err != io.EOF {
			return err
		}
		if n == 0 {
			break
		}

		if _, err := destination.Write(buf[:n]); err != nil {
			return err
		}
	}
	return err
}

func Copy(source, destination string) error {
	// log := clog.ZapLogger()

	if ok := FileExist(destination); ok {
		// file already exist
		DeleteFile(destination)
	}
	BUFFERSIZE, err := strconv.ParseInt("1000", 10, 64)
	if err != nil {
		// log.Error(err.Error())
		return err
	}

	// fmt.Printf("Copying %s to %s\n", source, destination)
	err = copy(source, destination, BUFFERSIZE)
	if err != nil {
		// log.Error(err.Error())
		return err
	}
	return nil
}

// // Copy recursively directories, symlinks, files copies from src to dst
// // Copy will make dirs as necessary, and keep file modes
// // Symlinks will be dereferenced similar to `cp -r src dst`
// func Copy(sourceFile, destinationFile string) error {
// //log := clog.ZapLogger()
// 	// // get source info
// 	// info, err := os.Lstat(src)
// 	// if err != nil {
// 	// 	return err
// 	// }
// 	// // make sure dest dir exists
// 	// if err := os.MkdirAll(filepath.Dir(dst), os.ModePerm); err != nil {
// 	// 	return err
// 	// }
// 	// // do real copy work
// 	// return copy(src, dst, info)

// 	input, err := ioutil.ReadFile(sourceFile)
// 	if err != nil {
// 		fmt.Println("Error reading source file for copy"+ err.Error())
// 		return err
// 	}

// 	err = ioutil.WriteFile(destinationFile, input, 0644)
// 	if err != nil {
// 		fmt.Println("Error creating", destinationFile)
// // 		log.Error(err.Error())
// 		return err
// 	}
// 	return nil
// }

// func copy(src, dst string, info os.FileInfo) error {
// // log := clog.ZapLogger()
// 	if info.Mode()&os.ModeSymlink != 0 {
// 		return copySymlink(src, dst)
// 	}
// 	if info.IsDir() {
// 		return copyDir(src, dst, info)
// 	}
// 	return copyFile(src, dst, info)
// }

// // CopyFile copies a file from src to dst
// func CopyFile(src, dst string) (err error) {
// // log := clog.ZapLogger()
// 	// get source information
// 	if _, Src := os.Stat(src); os.IsNotExist(Src) {
// 		return errors.New("Unable to copy file " + src + ". Source does not exist")
// 	}
// 	info, err := os.Stat(src)
// 	if err != nil {
// 		return err
// 	}
// 	return copyFile(src, dst, info)
// }

// func copyFile(src, dst string, info os.FileInfo) error {
// //log := clog.ZapLogger()
// 	// open src for reading
// 	in, err := os.Open(src)
// 	if err != nil {
// 		return err
// 	}
// 	defer in.Close()
// 	// create dst file
// 	// this is like f, err := os.Create(dst); os.Chmod(f.Name(), src.Mode())
// 	out, err := os.OpenFile(dst, os.O_RDWR|os.O_CREATE|os.O_TRUNC, info.Mode())
// 	if err != nil {
// 		return err
// 	}
// 	// make sure we close the file
// 	defer func() {
// 		closeErr := out.Close()
// 		// if we weren't returning an error
// 		if err == nil {
// 			err = closeErr
// 		}
// 	}()
// 	// actually copy
// 	if _, err = io.Copy(out, in); err != nil {
// 		return err
// 	}
// 	err = out.Sync()
// 	return err
// }

// // copySymlink dereferences and then copies a symlink
// func copySymlink(src, dst string) error {
// //log := clog.ZapLogger()
// 	// read through the symlink
// 	realSrc, err := filepath.EvalSymlinks(src)
// 	if err != nil {
// 		return err
// 	}
// 	info, err := os.Lstat(realSrc)
// 	if err != nil {
// 		return err
// 	}
// 	// copy the underlying contents
// 	return copy(realSrc, dst, info)
// }

// func copyDir(src, dst string, info os.FileInfo) error {
// //log := clog.ZapLogger()
// 	// make sure the target dir exists
// 	if err := os.MkdirAll(dst, info.Mode()); err != nil {
// 		return err
// 	}
// 	// copy every source dir entry
// 	entries, err := ioutil.ReadDir(src)
// 	if err != nil {
// 		return err
// 	}
// 	for _, entry := range entries {
// 		entrySrc := filepath.Join(src, entry.Name())
// 		entryDst := filepath.Join(dst, entry.Name())
// 		if err := copy(entrySrc, entryDst, entry); err != nil {
// 			return err
// 		}
// 	}
// 	return nil
// }

func ListFolders(d string) []string {
	// log := clog.ZapLogger()
	var dirs []string
	files, err := ioutil.ReadDir(d)
	if err != nil {
		// log.Error(err.Error())
	}

	for _, f := range files {
		fmt.Println(f.Name())
		if f.IsDir() == true {
			dirs = append(dirs, f.Name())
		}
	}
	return dirs
}
