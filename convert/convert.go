package convert

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var _regexpCommunity = regexp.MustCompile(`(\d+):(\d+)`)

// SedString : Find and replace strings
func SedString(find string, replace string, value string) string {

	result := strings.Replace(value, find, replace, -1)
	return result
}

func MatchesAny(regExps []string, s string) bool {
	for _, item := range regExps {
		if matched, _ := regexp.MatchString(item, s); matched {
			return true
		}
	}
	return false
}

// Return true if the given list contains the given element
func ListContainsElement(list []string, element string) bool {
	for _, item := range list {
		if item == element {
			return true
		}
	}

	return false
}

// Return a copy of the given list with all instances of the given element removed
func RemoveElementFromList(list []string, element string) []string {
	out := []string{}
	for _, item := range list {
		if item != element {
			out = append(out, item)
		}
	}
	return out
}

// Returns a copy of the given list with all duplicates removed (keeping the first encountereds)
func RemoveDuplicatesFromList(list []string) []string {
	return removeDuplicatesFromList(list, false)
}

// Returns a copy of the given list with all duplicates removed (keeping the last encountereds)
func RemoveDuplicatesFromListKeepLast(list []string) []string {
	return removeDuplicatesFromList(list, true)
}

func removeDuplicatesFromList(list []string, keepLast bool) []string {
	out := make([]string, 0, len(list))
	present := make(map[string]bool)

	for _, value := range list {
		if _, ok := present[value]; ok {
			if keepLast {
				out = RemoveElementFromList(out, value)
			} else {
				continue
			}
		}
		out = append(out, value)
		present[value] = true
	}
	return out
}

// CommaSeparatedStrings returns an HCL compliant formatted list of strings (each string within double quote)
func CommaSeparatedStrings(list []string) string {
	values := make([]string, 0, len(list))
	for _, value := range list {
		values = append(values, fmt.Sprintf(`"%s"`, value))
	}
	return strings.Join(values, ", ")
}

// Make a copy of the given list of strings
func CloneStringList(listToClone []string) []string {
	out := []string{}
	for _, item := range listToClone {
		out = append(out, item)
	}
	return out
}

// Make a copy of the given map of strings
func CloneStringMap(mapToClone map[string]string) map[string]string {
	out := map[string]string{}
	for key, value := range mapToClone {
		out[key] = value
	}
	return out
}

// A convenience method that returns the first item (0th index) in the given list or an empty string if this is an
// empty list
func FirstArg(args []string) string {
	if len(args) > 0 {
		return args[0]
	}
	return ""
}

// A convenience method that returns the second item (1st index) in the given list or an empty string if this is a
// list that has less than 2 items in it
func SecondArg(args []string) string {
	if len(args) > 1 {
		return args[1]
	}
	return ""
}

// A convenience method that returns the last item in the given list or an empty string if this is an empty list
func LastArg(args []string) string {
	if len(args) > 0 {
		return args[len(args)-1]
	}
	return ""
}

// StringToLowercase : Convert characters in a string to lowercase
func StringToLowercase(data string) string {
	var converted string
	if data != "" {
		converted = strings.ToLower(data)
	}
	return converted
}

// Convert byte[] to string
func ByteArrayToString(b []byte) string {
	s := fmt.Sprintf("%s", b)

	return s
}

func Int64ToString(n int64) string {
	s := strconv.FormatInt(n, 10)
	return s
}

// StringToInt64 : Convert string to int64
func StringToInt64(s string) int64 {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}

func IntToString(i int) string {
	return strconv.Itoa(i)
}

// HexToBin : Convert hex string to bin
func HexToBin(hex string) (string, error) {
	ui, err := strconv.ParseUint(hex, 16, 64)
	if err != nil {
		return "", err
	}

	// %016b indicates base 2, zero padded, with 16 characters
	return fmt.Sprintf("%016b", ui), nil
}

// StringToInt : Convert string to int
func StringToInt(s string) int {
	i1, err := strconv.Atoi(s)
	if err != nil {
		// fmt.Println(err, "stringToInt: ", s)
		return 0
	}
	return i1
}

// IntToAsn : Convert ints to []uint32s
func IntToAsn(asn int) []uint32 {
	peerASNs := make([]uint32, 0)
	peerASNs = append(peerASNs, uint32(asn))
	return peerASNs
}

func Uint32ToString(n uint32) string {
	return strings.Trim(fmt.Sprint(n), "\n")
}

// StringToIPs : Convert string to net.IP address
func StringToIPs(s string) ([]net.IP, error) {
	ips := make([]net.IP, 0)

	ip := net.ParseIP(s)
	if ip == nil {
		return nil, fmt.Errorf("Could not parse \"%s\" as an IP.", s)
	}
	ips = append(ips, ip)

	return ips, nil
}

func PrettyPrint(b []byte) ([]byte, error) {
	var out bytes.Buffer
	err := json.Indent(&out, b, "", "  ")
	return out.Bytes(), err
}

func PrettyPrintStruct(b interface{}) ([]byte, error) {
	var out bytes.Buffer
	jm, _ := json.Marshal(b)
	err := json.Indent(&out, jm, "", "  ")
	return out.Bytes(), err
}

func Before(value string, a string) string {
	// Get substring before a string.
	// Used to parse out the device name
	pos := strings.Index(value, a)
	if pos == -1 {
		return ""
	}
	return value[0:pos]
}

func Between(value string, a string, b string) string {
	// Get substring between two strings.
	// Used to parse out the full key string
	posFirst := strings.Index(value, a)
	if posFirst == -1 {
		return ""
	}
	posLast := strings.Index(value, b)
	if posLast == -1 {
		return ""
	}
	posFirstAdjusted := posFirst + len(a)
	if posFirstAdjusted >= posLast {
		return ""
	}
	return value[posFirstAdjusted:posLast]
}

func After(value string, a string) string {
	// Get substring after a string.
	pos := strings.LastIndex(value, a)
	if pos == -1 {
		return ""
	}
	adjustedPos := pos + len(a)
	if adjustedPos >= len(value) {
		return ""
	}
	return value[adjustedPos:len(value)]
}

func ConvertMemoryString(m string) int64 {
	var memory int64
	if strings.Contains(m, "g") {
		val := Before(m, "g")
		m = strings.Join([]string{val, "000000000"}, "")
	}
	if strings.Contains(m, "m") {
		val := Before(m, "m")
		m = strings.Join([]string{val, "000000"}, "")
	}
	if strings.Contains(m, "k") {
		val := Before(m, "k")
		m = strings.Join([]string{val, "000"}, "")
	}

	if m == "" {
		// fmt.Println("No memory settings detected: ", m)
	} else {
		memory = StringToInt64(m)
	}
	return memory
}

// ParseAs : Convert As from string to unint32
func ParseAs(arg string) uint32 {
	i, err := strconv.ParseUint(arg, 10, 32)
	if err == nil {
		return uint32(i)
	}

	elems := _regexpCommunity.FindStringSubmatch(arg)
	if len(elems) == 3 {
		fst, _ := strconv.ParseUint(elems[1], 10, 16)
		snd, _ := strconv.ParseUint(elems[2], 10, 16)
		return uint32(fst<<16 | snd)
	}
	return 0
}

func Int32ToString(i int64, base int) string {

	return fmt.Sprintf("%v", i)
}

func StringToUint32(arg string) uint32 {
	i, err := strconv.ParseUint(arg, 10, 32)
	if err == nil {
		return uint32(i)
	}

	elems := _regexpCommunity.FindStringSubmatch(arg)
	if len(elems) == 3 {
		fst, _ := strconv.ParseUint(elems[1], 10, 16)
		snd, _ := strconv.ParseUint(elems[2], 10, 16)
		return uint32(fst<<16 | snd)
	}
	return 0
}

func StringToInt32(arg string) int32 {
	i, err := strconv.ParseInt(arg, 10, 32)
	if err == nil {
		return int32(i)
	}

	return 0
}

func RemoveLeadingEmptyStringsInStringArray(sa []string) []string {
	firstNonEmptyStringIndex := 0
	for i := 0; i < len(sa); i++ {
		if sa[i] == "" {
			firstNonEmptyStringIndex++
		} else {
			break
		}
	}
	return sa[firstNonEmptyStringIndex:len(sa)]
}

func RemoveTrailingEmptyStringsInStringArray(sa []string) []string {
	lastNonEmptyStringIndex := len(sa) - 1
	for i := lastNonEmptyStringIndex; i >= 0; i-- {
		if sa[i] == "" {
			lastNonEmptyStringIndex--
		} else {
			break
		}
	}
	return sa[0 : lastNonEmptyStringIndex+1]
}

func Float64ToString(n float64) string {
	return fmt.Sprintf("%f", n)
}

func Float32ToString(n float32) string {
	return fmt.Sprintf("%f", n)
}

func StringToFloat64(s string) (float64, error) {
	return strconv.ParseFloat(s, 64)
}

func TimeToString(t time.Time) string {
	return t.String()
}
