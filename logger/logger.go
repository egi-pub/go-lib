package log

import (
	"os"
	"path/filepath"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func MkDir(dir string) error {
	if _, MkDirErr := os.Stat(dir); os.IsNotExist(MkDirErr) {
		os.MkdirAll(dir, 0755)
	}
	return nil
}

func NewLogger(path string) *zap.Logger {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		dir, _ := filepath.Split(path)
		MkDir(dir)
		var file, _ = os.Create(path)
		defer file.Close()
	}
	if os.Getenv("DEBUG") != "" {
		return DebugLogger(path)
	} else if os.Getenv("DEV") != "" {
		return DebugLogger(path)
	}
	return ProductionLogger(path)
}

func DebugLogger(logpath string) *zap.Logger {
	var errorLevel zapcore.Level
	if os.Getenv("DEBUG_LEVEL") == "" {
		os.Setenv("DEBUG_LEVEL", "2")
	}
	switch os.Getenv("DEBUG_LEVEL") {
	case "1":
		errorLevel = zapcore.FatalLevel
	case "2":
		errorLevel = zapcore.ErrorLevel
	case "3":
		errorLevel = zapcore.WarnLevel
	case "4":
		errorLevel = zapcore.DebugLevel
	case "5":
		errorLevel = zapcore.InfoLevel
	default:
		errorLevel = zapcore.ErrorLevel
	}
	cfg := zap.Config{
		Encoding: "json",
		Level:    zap.NewAtomicLevelAt(errorLevel),
		OutputPaths: []string{
			"stderr",
			logpath,
		},
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey: "message",

			LevelKey:    "level",
			EncodeLevel: zapcore.CapitalLevelEncoder,

			TimeKey: "time",
			// EncodeTime: zapcore.ISO8601TimeEncoder,
			EncodeTime: SyslogTimeEncoder,

			CallerKey: "caller",
			// EncodeCaller: zapcore.ShortCallerEncoder,
			EncodeCaller: zapcore.FullCallerEncoder,
		},
	}

	// cfg.EncoderConfig.EncodeTime = SyslogTimeEncoder
	cfg.EncoderConfig.EncodeLevel = CustomLevelEncoder

	logger, _ := cfg.Build()

	// writeFile := zapcore.AddSync(&lumberjack.Logger{
	// 	Filename:   "/var/log/cectl.log",
	// 	MaxSize:    500, // megabytes
	// 	MaxBackups: 3,
	// 	MaxAge:     28, // days
	// })

	// logger.WithOptions(
	// 	zap.WrapCore(
	// 		func(zapcore.Core) zapcore.Core {
	// 			return zapcore.NewCore(zapcore.NewConsoleEncoder(cfg.EncoderConfig), writeFile, errorLevel)
	// 		}))

	return logger
}

func ProductionLogger(logpath string) *zap.Logger {
	var errorLevel zapcore.Level

	switch os.Getenv("DEBUG_LEVEL") {
	case "1":
		errorLevel = zapcore.FatalLevel
	case "2":
		errorLevel = zapcore.ErrorLevel
	case "3":
		errorLevel = zapcore.WarnLevel
	case "4":
		errorLevel = zapcore.DebugLevel
	case "5":
		errorLevel = zapcore.InfoLevel
	default:
		errorLevel = zapcore.ErrorLevel
	}

	cfg := zap.Config{
		Encoding: "json",
		Level:    zap.NewAtomicLevelAt(errorLevel),
		OutputPaths: []string{
			"stderr",
			logpath,
		},
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey: "message",

			LevelKey:    "level",
			EncodeLevel: zapcore.CapitalLevelEncoder,

			TimeKey: "time",
			// EncodeTime: zapcore.ISO8601TimeEncoder,
			EncodeTime: SyslogTimeEncoder,

			CallerKey: "caller",
			// EncodeCaller: zapcore.ShortCallerEncoder,
			EncodeCaller: zapcore.FullCallerEncoder,
		},
	}

	// cfg.EncoderConfig.EncodeTime = SyslogTimeEncoder
	cfg.EncoderConfig.EncodeLevel = CustomLevelEncoder

	logger, _ := cfg.Build()

	// writeFile := zapcore.AddSync(&lumberjack.Logger{
	// 	Filename:   "/var/log/cectl.log",
	// 	MaxSize:    500, // megabytes
	// 	MaxBackups: 3,
	// 	MaxAge:     28, // days
	// })

	// logger.WithOptions(
	// 	zap.WrapCore(
	// 		func(zapcore.Core) zapcore.Core {
	// 			return zapcore.NewCore(zapcore.NewConsoleEncoder(cfg.EncoderConfig), writeFile, errorLevel)
	// 		})).Info("Logfile initialized")

	return logger
}

func SyslogTimeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	// enc.AppendString(t.Format("Jan  2 15:04:05"))
	enc.AppendString(t.Format(time.RFC1123))
	// enc.AppendString(t.Format("2006-01-02 15:04:05"))
}

func CustomLevelEncoder(level zapcore.Level, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString("[" + level.CapitalString() + "]")
}
