module gitlab.com/egi-pub/go-lib

go 1.13

require (
	github.com/araddon/dateparse v0.0.0-20200409225146-d820a6159ab1
	go.uber.org/zap v1.15.0
)
