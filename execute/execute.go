package execute

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
	"sync"
)

type ExecTask struct {
	Command string
	Args    []string
	Shell   bool
	Env     []string
	Cwd     string
	Input   bool
	// StreamStdio prints stdout and stderr directly to os.Stdout/err as
	// the command runs.
	StreamStdio bool

	// PrintCommand prints the command before executing
	PrintCommand bool
}

type ExecResult struct {
	Stdout   string
	Stderr   string
	ExitCode int
}

func (et ExecTask) ExecuteWg(wg *sync.WaitGroup) ExecResult {
	defer wg.Done()
	if os.Getenv("DEBUG") != "" {
		et.StreamStdio = true
	}
	r, _ := et.Execute()
	return r
}

func (et ExecTask) Execute() (ExecResult, error) {
	if os.Getenv("DEBUG") != "" {
		et.StreamStdio = true
		fmt.Println(et)
	}

	argsSt := ""
	if len(et.Args) > 0 {
		argsSt = strings.Join(et.Args, " ")
	}

	if et.PrintCommand || os.Getenv("DEBUG") != "" {
		fmt.Println("exec: ", et.Command, argsSt)
	}

	var cmd *exec.Cmd

	if et.Shell {
		var args []string
		if len(et.Args) == 0 {
			startArgs := strings.Split(et.Command, " ")
			script := strings.Join(startArgs, " ")
			args = append([]string{"--"}, fmt.Sprintf("%s", script))
			args = append([]string{"-c"}, fmt.Sprintf("%s", script))

		} else {
			script := strings.Join(et.Args, " ")
			args = append([]string{"--"}, fmt.Sprintf("%s %s", et.Command, script))
			args = append([]string{"-c"}, fmt.Sprintf("%s %s", et.Command, script))

		}

		if et.PrintCommand || os.Getenv("DEBUG") != "" {
			fmt.Println("cmd: ", "/bin/bash ", args)
		}

		cmd = exec.Command("/bin/bash", args...)
		if et.Input == true {
			cmd.Stdin = os.Stdin
		}
	} else {
		if strings.Index(et.Command, " ") > 0 {
			parts := strings.Split(et.Command, " ")
			command := parts[0]
			args := parts[1:]
			if et.PrintCommand || os.Getenv("DEBUG") != "" {
				fmt.Println("cmd: ", command, args)
			}
			cmd = exec.Command(command, args...)
			if et.Input == true {
				cmd.Stdin = os.Stdin
			}

		} else {
			if et.PrintCommand || os.Getenv("DEBUG") != "" {
				fmt.Println("cmd: ", et.Command, et.Args)
			}
			cmd = exec.Command(et.Command, et.Args...)
			if et.Input == true {
				cmd.Stdin = os.Stdin
			}

		}
	}

	cmd.Dir = et.Cwd

	if len(et.Env) > 0 {
		cmd.Env = os.Environ()
		for _, env := range et.Env {
			cmd.Env = append(cmd.Env, env)
		}
	}

	stdoutBuff := bytes.Buffer{}
	stderrBuff := bytes.Buffer{}

	var stdoutWriters io.Writer
	var stderrWriters io.Writer

	if et.StreamStdio {
		stdoutWriters = io.MultiWriter(os.Stdout, &stdoutBuff)
		stderrWriters = io.MultiWriter(os.Stderr, &stderrBuff)
	} else {
		stdoutWriters = &stdoutBuff
		stderrWriters = &stderrBuff
	}

	cmd.Stdout = stdoutWriters
	cmd.Stderr = stderrWriters

	startErr := cmd.Start()

	if startErr != nil {
		e := ExecResult{
			Stderr: startErr.Error(),
		}
		return e, startErr
	}

	exitCode := 0
	execErr := cmd.Wait()
	if execErr != nil {
		if exitError, ok := execErr.(*exec.ExitError); ok {

			exitCode = exitError.ExitCode()
		}
	}

	return ExecResult{
		Stdout:   strings.Trim(string(stdoutBuff.Bytes()), "\n"),
		Stderr:   string(stderrBuff.Bytes()),
		ExitCode: exitCode,
	}, nil
}
